import React, {Component} from 'react';

class LinkCreate extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: ''
    };
  }

  handleSubmit(event) {
    event.preventDefault();
    let url = this.refs.textbox.value;

    Meteor.call('links.insert', url, (error) => {
      if (error) {
        this.setState({error: 'Enter valid URL'});
      } else {
        this.setState({error: ''});
        this.refs.textbox.value = '';
      }
    });
  }

  render() {
    return (<form onSubmit={this.handleSubmit.bind(this)}>
      <div className="form-group">
        <label>
          Link to shorten
        </label>
        <input className="form-control" ref="textbox"/>
      </div>
      <div className="text-danger">{this.state.error}</div>
      <button className="btn btn-primary">Shorten!</button>
    </form>);
  }
};

export default LinkCreate;
