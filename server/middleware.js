import ConnectRoute from 'connect-route';

import { Links } from '../imports/collections/links';



// URL Redirect middleware
function onRoute(req,res,next){
  const link = Links.findOne({token:req.params.token});

  if(link){

    // Update click count before redirecting
    Links.update(link,{ $inc:{clicks:1}});

    // send response with redirect to original URL
    res.writeHead(307,{'Location':link.url});
    res.end();

  }else{
    // Load our App if token not found or invalid
    next();
  }
}

export const middleware = ConnectRoute(function(router) {
  router.get('/:token', onRoute);
});
