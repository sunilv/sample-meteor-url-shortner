import { Meteor } from 'meteor/meteor';
import { WebApp } from 'meteor/webapp';

import { Links } from '../imports/collections/links';
import {middleware} from './middleware';


Meteor.startup(()=>{
      Meteor.publish('links',()=> Links.find({}));
});

WebApp.connectHandlers
    .use(middleware);
